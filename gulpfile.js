
//less => css


var gulp = require('gulp');
var less = require('gulp-less');

gulp.task('lesstocss', function () {
    gulp.src('./css/**/*.less')
        .pipe(less())
        .pipe(gulp.dest('./public/less_to_css/'))
    ;
});


//concat-css


var gulp = require('gulp');
var concatCss = require('gulp-concat-css');
 
gulp.task('concat-css', function () {
  return gulp.src('./public/less_to_css/*.css')
    .pipe(concatCss("bundle.css"))
    .pipe(gulp.dest('./public/CSS-concat/'));
});


//minify-css


var gulp = require('gulp');
var cleanCSS = require('gulp-clean-css');

gulp.task('minify-css', function() {
  return gulp.src('./public/CSS-concat/*.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('./public/CSS-min/'));
});


//Compressing JS


var minify = require('gulp-minify');
 
gulp.task('compressJS', function() {
  gulp.src('lib/*.js')
    .pipe(minify({
        ext:{
            src:'-debug.js',
            min:'.js'
        },
        exclude: ['tasks'],
        ignoreFiles: ['.combo.js', '-min.js']
    }))
    .pipe(gulp.dest('./public/JS-min/'))
});